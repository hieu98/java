package com.digidinos.shopping.service;


import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.digidinos.shopping.entity.OrderDetail;
import com.digidinos.shopping.repository.OrderDetailRepository;

@Service
public class OrderDetailService {

	@Autowired
	OrderDetailRepository orderDetailRepository;
	
	public OrderDetail findById(int id) {
		
		List<OrderDetail> list = orderDetailRepository.findById(id);
		
		if(list.size() == 0) return null;
		else return (OrderDetail) list.get(0);
		
	}
	
	public void save(OrderDetail orderDetail) {
		orderDetailRepository.save(orderDetail);
	}
	
	public OrderDetail delete(int id) {
		
		OrderDetail orderDetail = (OrderDetail) orderDetailRepository.findById(id).get(0);
		
		orderDetail.setDeleted(true);
		orderDetail.setDeletedAt(new Date());
		
		this.save(orderDetail);
		
		return orderDetail;
	}
	
	public int getMaxId() {
		
		int maxId= 0;
		List<OrderDetail> list = orderDetailRepository.findAll();
		if (list.size() != 0) {
			maxId = ((OrderDetail) list.get(list.size() - 1)).getId();
		}

		return maxId;
	}
}

