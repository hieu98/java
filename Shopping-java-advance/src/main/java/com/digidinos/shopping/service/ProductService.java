package com.digidinos.shopping.service;

import java.io.IOException;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.digidinos.shopping.entity.Product;
import com.digidinos.shopping.form.ProductForm;
import com.digidinos.shopping.model.ProductInfo;
import com.digidinos.shopping.pagination.PaginationResult;
import com.digidinos.shopping.repository.ProductRepository;

@Service
public class ProductService {
	
	@Autowired
	EntityManager em;

	@Autowired
	ProductRepository productRepository;
	
	public void save(Product product) {
		productRepository.save(product);
	}
	
	public int getMaxId() {
		
		int maxId = -1;
		
		List<Product> list = productRepository.findAll();
		
		if(list.size() > 0) maxId = list.get(list.size() - 1).getId();
		
		return maxId;
	}
	
	public Product findById(int id) {
		List<Product> list = productRepository.findById(id);
		if(list.size() == 0) return null;
		else return (Product) list.get(0);
	}
	
	public Product findByCode(String code) {
		List<Product> list = productRepository.findById(code);
		if(list.size() == 0) return null;
		else return (Product) list.get(0);
	}
	
	public List<Product> findAll(){
		return productRepository.findAll();
	}
	
	public Product delete(int id) {
		
		Product product = productRepository.findById(id).get(0);
		product.setDeleted(true);
		product.setDeletedAt(new Date());
		productRepository.save(product);
		
		return (Product) product;
	}
	
	public PaginationResult<ProductInfo> queryProduct(int page, int maxResult, int maxNavigationPage){
		
		String sql = "Select new " + ProductInfo.class.getName() //
                + "(p.code, p.name, p.price) " + " from "//
                + Product.class.getName() + " p " + " ORDER BY p.createdAt DESC";
		TypedQuery<ProductInfo> query = em.createQuery(sql, ProductInfo.class);
		
		return  new PaginationResult<ProductInfo>((Query<ProductInfo>) query, page, maxResult, maxNavigationPage);
	}
	
	public PaginationResult<ProductInfo> queryProducts(int page, int maxResult, int maxNavigationPage) {
        return queryProduct(page, maxResult, maxNavigationPage);
    }
	
	public Product saveByForm(ProductForm productForm) {
		
		String code = productForm.getCode();
		
		Product product = this.findByCode(code);
		
		if(product != null) {
			product.setUpdatedAt(new Date());
		}
		
		if(product == null) {
			product = new Product();
			product.setId(this.getMaxId() + 1);
			product.setCreatedAt(new Date());
		}
		
		product.setCode(code);
		try {
			product.setImage(productForm.getFileData().getBytes());
		}
		catch(IOException e) {
			product.setImage((byte[]) null);
		}
		
		product.setName(productForm.getName());
		product.setPrice(productForm.getPrice());
		product.setDeleted(false);
		
		productRepository.save(product);
		
		return product;
	}
}

